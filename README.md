# xmpp-spammer

xmpp-spammer is a tool to bulk message xmpp messages to a provided list of users.


## Usage

Please create the configuration file "config.yaml" and add your username and password and the message body to send:

``` yaml
user: "test@exploit.im"
password: "test"
server: "exploit.im:5222"
bodyMessageText: "hello"
recipientsList: "recipients.txt"

```

Create a "recipients.txt" with the addresses that will receive the messages:

```
test@jabber.im
test2@jabber.com
```

Important: Both files need to be in the same path or folder as the executable!

Run with

```
go run cmd/main.go
```

## Build

```
go build cmd/main.go
```

## limitations

xmpp providers nay limit the messages that can be sent

