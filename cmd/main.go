package main

import (
	"io/ioutil"
	"log"
	"xmpp-marketer/internal/textfileservice"
	"xmpp-marketer/internal/xmppservice"

	"gopkg.in/yaml.v2"
)

type Config struct {
	User       string `yaml:"user"`
	Password   string `yaml:"password"`
	Body       string `yaml:"bodyMessageText"`
	Server     string `yaml:"server"`
	Recipients string `yaml:"recipientsList"`
}

func main() {
	yamlFile, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Fatalf("Error reading YAML file: %s\n", err)
	}

	var yamlConfig Config
	err = yaml.Unmarshal(yamlFile, &yamlConfig)

	if err != nil {
		log.Fatalf("Error parsing YAML file: %s\n", err)
	}

	lines, err := textfileservice.ReadFile(yamlConfig.Recipients)
	if err != nil {
		log.Fatal(err)
	}

	xmppservice.XMPPLogic(lines, yamlConfig.Body, yamlConfig.Server, yamlConfig.User, yamlConfig.Password)
}
