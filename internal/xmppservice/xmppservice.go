package xmppservice

import (
	"log"
	"time"

	"gosrc.io/xmpp"
	"gosrc.io/xmpp/stanza"
)

func XMPPLogic(lines []string, body string, addr string, user string, passwd string) {
	config := &xmpp.Config{
		TransportConfiguration: xmpp.TransportConfiguration{
			Address: addr,
		},
		Jid:          user,
		Credential:   xmpp.Password(passwd),
		StreamLogger: nil,
		Insecure:     true,
		// TLSConfig: tls.Config{InsecureSkipVerify: true},
	}

	router := xmpp.NewRouter()
	router.HandleFunc("message", HandleMessage)

	client, err := xmpp.NewClient(config, router, ErrorHandler)
	if err != nil {
		log.Fatalf("%+v", err)
	}

	err = client.Connect()

	if err != nil {
		log.Fatal(err)
	}

	for i := range lines {
		log.Println("sending message to", lines[i])
		err = SendMessage(lines[i], body, client)

		if err != nil {
			log.Println(err)
		}

		time.Sleep(time.Millisecond * 100)
	}
}

func HandleMessage(s xmpp.Sender, p stanza.Packet) {
	// do nothing with received messages
}

func ErrorHandler(err error) {
	log.Println(err)
}

func SendMessage(recipient string, body string, client *xmpp.Client) error {
	reply := stanza.Message{Attrs: stanza.Attrs{To: recipient, Type: stanza.MessageTypeChat}, Body: body}
	err := client.Send(reply)

	if err != nil {
		return err
	}

	return nil
}
