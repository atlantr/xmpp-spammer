package textfileservice

import (
	"bufio"
	"os"
	"strings"
)

func ReadFile(fileName string) ([]string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		s := scanner.Text()
		lines = append(lines, strings.TrimSpace(s))
	}

	linesCleaned := RemoveDuplicatesFromSlice(lines)

	return linesCleaned, nil
}

func RemoveDuplicatesFromSlice(s []string) []string {
	m := make(map[string]bool)

	for _, item := range s {
		if _, ok := m[item]; ok {
		} else {
			m[item] = true
		}
	}

	result := make([]string, 0, len(m))

	for item := range m {
		result = append(result, item)
	}

	return result
}
